package org.torproject.descriptor.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.torproject.descriptor.DescriptorParseException;
import org.torproject.descriptor.GeoipFile;

import org.apache.commons.compress.utils.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class GeoipFileImplTest {

  @Test
  public void testParseGeoIP4() throws IOException, DescriptorParseException {
    LinkedList<GeoipFile.GeoipEntry> expectedResults = new LinkedList<>();
    expectedResults.add(
            new GeoipEntryImpl("1.0.0.0", "1.0.0.255", "AU", null));
    expectedResults.add(
            new GeoipEntryImpl("1.0.1.0", "1.0.3.255", "CN", null));
    expectedResults.add(
            new GeoipEntryImpl("1.0.4.0", "1.0.7.255", "AU", null));
    expectedResults.add(
            new GeoipEntryImpl("1.0.8.0", "1.0.15.255", "CN", null));
    expectedResults.add(
            new GeoipEntryImpl("1.0.16.0", "1.0.31.255", "JP", null));
    URL resource = getClass().getClassLoader().getResource(
            "geoip/geoip");
    assertNotNull(resource);
    InputStream dataInputStream = resource.openStream();
    assertNotNull(dataInputStream);
    byte[] rawDescriptorBytes = IOUtils.toByteArray(dataInputStream);
    GeoipFile geoipFile = new GeoipFileImpl(rawDescriptorBytes,
            new int[]{0, rawDescriptorBytes.length}, null);
    List<GeoipFile.GeoipEntry> entries = geoipFile.getEntries();
    assertFalse(entries.isEmpty());
    for (GeoipFile.GeoipEntry e : entries) {
      if (expectedResults.isEmpty()) {
        break;
      }
      assertEquals(expectedResults.remove(), e);
    }
    /* first entry */
    assertEquals("AU", geoipFile.getEntry(
            InetAddress.getByName("1.0.0.0")).get().getCountryCode());
    assertEquals("AU", geoipFile.getEntry(
            InetAddress.getByName("1.0.0.1")).get().getCountryCode());
    assertEquals("AU", geoipFile.getEntry(
            InetAddress.getByName("1.0.0.255")).get().getCountryCode());
    /* some middle entry */
    assertEquals("RU", geoipFile.getEntry(
            InetAddress.getByName("217.194.240.0")).get().getCountryCode());
    assertEquals("RU", geoipFile.getEntry(
            InetAddress.getByName("217.194.242.100")).get().getCountryCode());
    assertEquals("RU", geoipFile.getEntry(
            InetAddress.getByName("217.194.255.255")).get().getCountryCode());
    /* last entry */
    assertEquals("AU", geoipFile.getEntry(
            InetAddress.getByName("223.255.255.0")).get().getCountryCode());
    assertEquals("AU", geoipFile.getEntry(
            InetAddress.getByName("223.255.255.128")).get().getCountryCode());
    assertEquals("AU", geoipFile.getEntry(
            InetAddress.getByName("223.255.255.255")).get().getCountryCode());
    assertTrue(geoipFile.getUnrecognizedLines() == null
            || geoipFile.getUnrecognizedLines().isEmpty());
  }

  @Test
  public void testParseGeoIP4WithAsn()
          throws IOException, DescriptorParseException {
    LinkedList<GeoipFile.GeoipEntry> expectedResults = new LinkedList<>();
    expectedResults.add(
            new GeoipEntryImpl("1.0.0.0", "1.0.0.255", "AU", "13335"));
    expectedResults.add(
            new GeoipEntryImpl("1.0.1.0", "1.0.3.255", "CN", "0"));
    expectedResults.add(
            new GeoipEntryImpl("1.0.4.0", "1.0.7.255", "AU", "38803"));
    URL resource = getClass().getClassLoader().getResource(
            "geoip/geoip-plus-asn");
    assertNotNull(resource);
    InputStream dataInputStream = resource.openStream();
    assertNotNull(dataInputStream);
    byte[] rawDescriptorBytes = IOUtils.toByteArray(dataInputStream);
    GeoipFile geoipFile = new GeoipFileImpl(rawDescriptorBytes,
            new int[]{0, rawDescriptorBytes.length}, null);
    List<GeoipFile.GeoipEntry> entries = geoipFile.getEntries();
    assertFalse(entries.isEmpty());
    for (GeoipFile.GeoipEntry e : entries) {
      if (expectedResults.isEmpty()) {
        break;
      }
      assertEquals(expectedResults.remove(), e);
    }
    assertTrue(geoipFile.getUnrecognizedLines() == null
            || geoipFile.getUnrecognizedLines().isEmpty());
  }

  @Test
  public void testParseGeoIP6() throws IOException, DescriptorParseException {
    LinkedList<GeoipFile.GeoipEntry> expectedResults = new LinkedList<>();
    expectedResults.add(
            new GeoipEntryImpl(
                    "2001:200::", "2001:200:134:ffff:ffff:ffff:ffff:ffff",
                    "JP", null));
    expectedResults.add(
            new GeoipEntryImpl(
                    "2001:200:135::", "2001:200:135:ffff:ffff:ffff:ffff:ffff",
                    "US", null));
    expectedResults.add(
            new GeoipEntryImpl(
                    "2001:200:136::", "2001:200:179:ffff:ffff:ffff:ffff:ffff",
                    "JP", null));
    expectedResults.add(
            new GeoipEntryImpl(
                    "2001:200:17a::", "2001:200:17b:ffff:ffff:ffff:ffff:ffff",
                    "US", null));
    expectedResults.add(
            new GeoipEntryImpl(
                    "2001:200:17c::", "2001:200:ffff:ffff:ffff:ffff:ffff:ffff",
                    "JP", null));
    URL resource = getClass().getClassLoader().getResource(
            "geoip/geoip6");
    assertNotNull(resource);
    InputStream dataInputStream = resource.openStream();
    assertNotNull(dataInputStream);
    byte[] rawDescriptorBytes = IOUtils.toByteArray(dataInputStream);
    GeoipFile geoipFile = new GeoipFileImpl(rawDescriptorBytes,
            new int[]{0, rawDescriptorBytes.length}, null);
    List<GeoipFile.GeoipEntry> entries = geoipFile.getEntries();
    assertFalse(entries.isEmpty());
    for (GeoipFile.GeoipEntry e : entries) {
      GeoipFile.GeoipEntry expected = expectedResults.remove();
      assertEquals(expected, e);
      if (expectedResults.isEmpty()) {
        break;
      }
    }
    /* first entry */
    assertEquals("JP", geoipFile.getEntry(
            InetAddress.getByName("2001:200::0000")).get().getCountryCode());
    assertEquals("JP", geoipFile.getEntry(
            InetAddress.getByName(
                    "2001:200:134::cafe")).get().getCountryCode());
    assertEquals("JP", geoipFile.getEntry(
            InetAddress.getByName(
                    "2001:200:134:ffff:ffff:ffff:ffff:ffff")).get()
            .getCountryCode());
    /* some middle entry */
    assertEquals("ZA", geoipFile.getEntry(
            InetAddress.getByName("2c0f:f598:0007::")).get().getCountryCode());
    assertEquals("ZA", geoipFile.getEntry(
            InetAddress.getByName("2c0f:f598:ffff:ffff::0000")).get()
            .getCountryCode());
    assertEquals("ZA", geoipFile.getEntry(
            InetAddress.getByName(
                    "2c0f:f598:ffff:ffff:ffff:ffff:ffff:ffff")).get()
            .getCountryCode());
    /* last entry */
    assertEquals("MU", geoipFile.getEntry(
            InetAddress.getByName("2c0f:fff1::0000")).get().getCountryCode());
    assertEquals("MU", geoipFile.getEntry(
            InetAddress.getByName("2c0f:fff1:face::1:1234")).get()
            .getCountryCode());
    assertEquals("MU", geoipFile.getEntry(
            InetAddress.getByName(
                    "2c0f:ffff:ffff:ffff:ffff:ffff:ffff:ffff")).get()
            .getCountryCode());
    assertTrue(geoipFile.getUnrecognizedLines() == null
            || geoipFile.getUnrecognizedLines().isEmpty());
  }

  @Test
  public void testParseGeoIP6WithAsn()
          throws IOException, DescriptorParseException {
    LinkedList<GeoipFile.GeoipEntry> expectedResults = new LinkedList<>();
    expectedResults.add(
            new GeoipEntryImpl("2001::",
                    "2001:0:ffff:ffff:ffff:ffff:ffff:ffff", "??", "6939"));
    expectedResults.add(
            new GeoipEntryImpl("2001:4:112::",
                    "2001:4:112:ffff:ffff:ffff:ffff:ffff", "??", "112"));
    expectedResults.add(
            new GeoipEntryImpl("2001:200::",
                    "2001:200:134:ffff:ffff:ffff:ffff:ffff", "JP", "2500"));
    URL resource = getClass().getClassLoader().getResource(
            "geoip/geoip6-plus-asn");
    assertNotNull(resource);
    InputStream dataInputStream = resource.openStream();
    assertNotNull(dataInputStream);
    byte[] rawDescriptorBytes = IOUtils.toByteArray(dataInputStream);
    GeoipFile geoipFile = new GeoipFileImpl(rawDescriptorBytes,
            new int[]{0, rawDescriptorBytes.length}, null);
    List<GeoipFile.GeoipEntry> entries = geoipFile.getEntries();
    assertFalse(entries.isEmpty());
    for (GeoipFile.GeoipEntry e : entries) {
      if (expectedResults.isEmpty()) {
        break;
      }
      assertEquals(expectedResults.remove(), e);
    }
    assertTrue(geoipFile.getUnrecognizedLines() == null
            || geoipFile.getUnrecognizedLines().isEmpty());
  }
}
